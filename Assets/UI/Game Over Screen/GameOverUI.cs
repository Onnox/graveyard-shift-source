using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOverUI : MonoBehaviour {

	[SerializeField] ScoreBag scoreBag;
	[Space]
	[SerializeField] ScoreBag highScoreBag;
	[Space]
	[SerializeField] TextMeshProUGUI congratsText;
	[SerializeField] string congratsTextValue;
	[SerializeField] string condolencesTextValue;

	AudioSource highScoreAudioSource;

	bool isHighScore = false;

	void Awake() {
		highScoreAudioSource = GetComponent<AudioSource>();
	}

	public void SetYourScoreText(int score) {
		scoreBag.SetScore(score);
	}

	public void SetHighScoreText(int score) {
		highScoreBag.SetScore(score);
	}

	public void SetHighScoreState(bool isHighScore) {
		congratsText.text = isHighScore ? congratsTextValue : condolencesTextValue;
		scoreBag.SetStarActive(isHighScore);
		highScoreBag.SetStarActive(isHighScore);
		this.isHighScore = isHighScore;
	}

	public void PlayHighScoreSoundIfNeeded() {
		if (isHighScore) {
			highScoreAudioSource.PlayIfNotMuted();
		}
	}
}
