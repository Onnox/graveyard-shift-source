using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class UIRotate : MonoBehaviour {

	[SerializeField] float rotateSpeed = 1f;

	RectTransform rectTransform;

	private void Awake() {
		rectTransform = GetComponent<RectTransform>();
	}

	void Update() {
		rectTransform.Rotate(0f, 0f, rotateSpeed * Time.unscaledDeltaTime);
	}
}
