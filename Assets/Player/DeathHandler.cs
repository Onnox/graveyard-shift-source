using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathHandler : MonoBehaviour {

	[SerializeField] float gameOverFadeSpeed = 1f;
	[SerializeField] CanvasFadeInOut gameOverCanvasFade;
	[SerializeField] GameRestart gameRestart;
	[SerializeField] CanvasFadeInOut audioCanvasFade;
	[SerializeField] CanvasFadeInOut scoreCanvasFade;
	[SerializeField] TutorialSequence tutorialSequence;
	[SerializeField] PauseHandler pauseHandler;
	[SerializeField] Animator animator;
	[Space]
	[SerializeField] float gameOverDelay = 2f;
	[SerializeField] GameOverUI gameOverUI;

	[Header("Game Over Audio")]
	[SerializeField] AudioSource nonDirectionalAudioSource;
	[SerializeField] AudioClip gameOverAudioClip;
	[SerializeField] float volume = 0.25f;

#if DEBUG
	[Header("Debug")]
	[SerializeField] bool godMode;
#endif

	AudioSource playerDeathAudioSource;
	PlayerMovement playerMovement;

	bool isDead = false;

	void Awake() {
		playerDeathAudioSource = GetComponent<AudioSource>();
		playerMovement = GetComponent<PlayerMovement>();
	}

	void Start() {
		gameOverCanvasFade.StartFadeOut(Mathf.Infinity);
	}

#if DEBUG
	void Update() {
		if (Input.GetKeyDown(KeyCode.G)) {
			godMode = !godMode;
		}
	}
#endif

	public void HandleDeath() {

#if DEBUG
		if (godMode) return;
#endif
		if (isDead) return;
		isDead = true;
		StartCoroutine(DoDeath(gameOverDelay));
	}

	IEnumerator DoDeath(float delay) {
		PlayerPrefs.Save();
		pauseHandler.SetCanPause(false);
		AnimateDeath();
		yield return new WaitForSecondsRealtime(delay);
		AnimateUI();
		yield return new WaitForSecondsRealtime(delay);
		gameOverUI.PlayHighScoreSoundIfNeeded();
		gameRestart.SetCanRestart(true);
	}

	void AnimateDeath() {
		playerMovement.enabled = false;
		EnemyMovement.DisableAllEnemyMovementAndStartTaunt();
		animator.SetTrigger(Constants.AnimatorParams.Player.death);
		playerDeathAudioSource.PlayIfNotMuted();
	}

	void AnimateUI() {
		gameOverCanvasFade.StartFadeIn(gameOverFadeSpeed);
		Time.timeScale = 0f;
		audioCanvasFade.StartFadeIn(gameOverFadeSpeed);
		scoreCanvasFade.StartFadeOut(gameOverFadeSpeed);
		nonDirectionalAudioSource.volume = volume;
		nonDirectionalAudioSource.PlayOneShotIfNotMuted(gameOverAudioClip);
		tutorialSequence.HideAllTutorialPrompts();
	}
}
