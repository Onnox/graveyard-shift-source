using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneFade : MonoBehaviour {

    [SerializeField] float fadeSpeed = 1f;

    CanvasFadeInOut canvasFade;

    void Start() {
        // Note: This must happen after CanvasFadeInOut in script execution order!
        canvasFade = GetComponent<CanvasFadeInOut>();
        canvasFade.StartFadeOut(fadeSpeed);
    }

    public void FadeIn(Action onCompleted) {
        canvasFade.StartFadeIn(fadeSpeed, onCompleted);
	}
}
