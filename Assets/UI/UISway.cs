using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISway : MonoBehaviour {

	[SerializeField] float swaySpeed = 1f;
	[SerializeField] float swayIntensity = 2f;

	RectTransform rectTransform;

	float offset;

	void Awake() {
		offset = Random.Range(0f, 2f * Mathf.PI);
		rectTransform = GetComponent<RectTransform>();
	}

	void Update() {
		rectTransform.rotation = Quaternion.Euler(0f, 0f, swayIntensity * Mathf.Sin(Time.realtimeSinceStartup * swaySpeed + offset));
	}
}
