using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class Enemy : MonoBehaviour {

	readonly string deathTriggerParam = Constants.AnimatorParams.Enemy.death;
	readonly string summonTriggerParam = Constants.AnimatorParams.Enemy.summon;

	[SerializeField] Animator animator;
	[SerializeField] float deathDuration;

	[Header("Audio")]
	[SerializeField] AudioClip summonSound;
	[SerializeField] AudioClip deathSound;

	IObjectPool<Enemy> pool;
	public IObjectPool<Enemy> Pool { set { pool = value; } }

	EnemyMovement enemyMovement;
	AudioSource audioSource;
	TutorialSequence tutorialSequence;

	void Awake() {
		enemyMovement = GetComponent<EnemyMovement>();
		audioSource = GetComponent<AudioSource>();
		tutorialSequence = FindObjectOfType<TutorialSequence>();
	}

	void OnEnable() {
		animator.SetTrigger(summonTriggerParam);
		audioSource.PlayOneShotIfNotMuted(summonSound);
	}

	public void Kill() {
		StartCoroutine(DoDeath());
		audioSource.PlayOneShotIfNotMuted(deathSound);
		tutorialSequence.SetHasKilledEnemies();
	}

	IEnumerator DoDeath() {
		enemyMovement.DisableMovement();
		SetCollidersEnabled(false);
		animator.SetTrigger(deathTriggerParam);
		yield return new WaitForSeconds(deathDuration);
		pool.Release(this);
	}

	public void SetCollidersEnabled(bool enabled) {
		Collider[] colliders = GetComponentsInChildren<Collider>();
		foreach (Collider collider in colliders) {
			collider.enabled = enabled;
		}
	}
}
