using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreBoard : MonoBehaviour {

	[SerializeField] ScoreBag scoreBagUI;
	//[SerializeField] TextMeshProUGUI scoreText;
	//[SerializeField] UIPing scorePing;
	[SerializeField] GameOverUI gameOverUI;
	[SerializeField] TutorialSequence tutorialSequence;

	UIPing scorePing;

	private void Awake() {
		scorePing = scoreBagUI.GetComponent<UIPing>();
	}

	int currentScore = 0;
	public int CurrentScore {
		get { return currentScore; }
		private set {
			currentScore = value;
			UpdateScoreUI();
			UpdateHighScoreIfNeeded();
		}
	}

	public void IncrementScore() {
		CurrentScore++;
		tutorialSequence.SetHasCollectedLoot();
	}

	void UpdateScoreUI() {
		scoreBagUI.SetScore(CurrentScore);
		scorePing.Ping();
		gameOverUI.SetYourScoreText(currentScore);
	}

	void UpdateHighScoreIfNeeded() {

		int highScore = PlayerPrefs.GetInt(Constants.PlayerPrefs.highScore, 0);

		bool isNewHighScore = CurrentScore > highScore;

		if (isNewHighScore) {
			highScore = CurrentScore;
			PlayerPrefs.SetInt(Constants.PlayerPrefs.highScore, CurrentScore);
		}

		gameOverUI.SetHighScoreText(highScore);
		gameOverUI.SetHighScoreState(isNewHighScore);
	}
}
