using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants {

	public struct AnimatorParams {

		public struct Player {
			public static string isRunning = "isRunning";
			public static string runAmount = "runAmount";
			public static string death = "death";
		}

		public struct Enemy {
			public static string runAmount = "runAmount";
			public static string death = "death";
			public static string summon = "summon";
			public static string attack = "attack";
			public static string taunt = "taunt";
		}

		public struct UI {
			public static string ping = "ping";
		}
	}

	public struct Input {
		public static string horizontal = "Horizontal";
		public static string vertical = "Vertical";
		public static string submit = "Submit";
		public static string pause = "Pause";
		public static string mute = "Mute";
	}

	public struct PlayerPrefs {
		public static string highScore = "HighScore";
	}

	public struct Tags {
		public static string environment = "Environment";
		public static string player = "Player";
	}
}
