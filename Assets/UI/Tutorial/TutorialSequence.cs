using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialSequence : MonoBehaviour {

	[SerializeField] float fadeSpeed = 1f;
	[SerializeField] CanvasGroup lootCanvasGroup;
	[SerializeField] CanvasGroup enemiesCanvasGroup;

	TutorialState state;
	CanvasGroup masterCanvasGroup;

	public bool ShouldStartEnemyTutorialSequence { 
		get { return state.ShouldStartEnemyTutorialSequence; } 
	}

	void Start() {
		masterCanvasGroup = GetComponent<CanvasGroup>();
		state = FindObjectOfType<TutorialState>();
	}

	public void StartLootTutorialSequence() {
		StartCoroutine(DoLootTutorialSequence());
	}

	public void StartEnemyTutorialSequence() {
		StartCoroutine(DoEnemyTutorialSequence());
	}

	public void HideAllTutorialPrompts() {
		StartCoroutine(FadeOutCanvasGroup(masterCanvasGroup));
	}

	public void SetHasCollectedLoot() {
		state.HasCollectedLoot = true;
	}

	public void SetHasKilledEnemies() {
		state.HasKilledEnemies = true;
	}

	IEnumerator DoLootTutorialSequence() {
		yield return StartCoroutine(FadeInCanvasGroup(lootCanvasGroup));
		yield return StartCoroutine(WaitForLootCollect());
		yield return StartCoroutine(FadeOutCanvasGroup(lootCanvasGroup));
	}

	IEnumerator DoEnemyTutorialSequence() {
		yield return StartCoroutine(FadeInCanvasGroup(enemiesCanvasGroup));
		yield return StartCoroutine(WaitForEnemyKill());
		yield return StartCoroutine(FadeOutCanvasGroup(enemiesCanvasGroup));
	}

	IEnumerator FadeInCanvasGroup(CanvasGroup canvasGroup) {

		canvasGroup.gameObject.SetActive(true);

		while (canvasGroup.alpha < 1f) {
			canvasGroup.alpha += fadeSpeed * Time.unscaledDeltaTime;
			yield return new WaitForEndOfFrame();
		}
	}

	IEnumerator FadeOutCanvasGroup(CanvasGroup canvasGroup) {

		while (canvasGroup.alpha > 0f) {
			canvasGroup.alpha -= fadeSpeed * Time.unscaledDeltaTime;
			yield return new WaitForEndOfFrame();
		}

		canvasGroup.gameObject.SetActive(false);
	}

	IEnumerator WaitForLootCollect() {
		while (!state.HasCollectedLoot) {
			yield return new WaitForEndOfFrame();
		}
	}

	IEnumerator WaitForEnemyKill() {
		while (!state.HasKilledEnemies) {
			yield return new WaitForEndOfFrame();
		}
	}
}
