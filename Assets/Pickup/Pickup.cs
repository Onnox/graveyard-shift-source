using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

[RequireComponent(typeof(Collider))]
public class Pickup : MonoBehaviour {

	[SerializeField] GameObject spawnParticleFxPrefab;
	[SerializeField] GameObject collectParticleFxPrefab;
	[SerializeField] float particleFxDuration;

	IObjectPool<Pickup> pool;
	public IObjectPool<Pickup> Pool { set { pool = value; } }

	RandomPickupMesh meshRandomiser;
	AudioRandomiser spawnAudioRandomiser;
	EnemySpawner enemySpawner;
	PickupSpawner pickupSpawner;
	
	void Awake() {
		meshRandomiser = GetComponent<RandomPickupMesh>();
		spawnAudioRandomiser = GetComponent<AudioRandomiser>();
		enemySpawner = FindObjectOfType<EnemySpawner>();
		pickupSpawner = GetComponentInParent<PickupSpawner>();
	}

	void OnTriggerEnter(Collider other) {

		ScoreBoard bodyBag = other.GetComponent<ScoreBoard>();
		if (bodyBag == null) return;

		bodyBag.IncrementScore();

		SpawnEnemy();
		CreateParticleFx(collectParticleFxPrefab);
		DestroyPickup();
		SpawnNewPickup();
	}

	public void CreateSpawnParticleFx() {
		spawnAudioRandomiser.PlayRandomAudioClip();
		CreateParticleFx(spawnParticleFxPrefab);
	}

	void SpawnEnemy() {
		enemySpawner.SpawnEnemy();
	}

	void CreateParticleFx(GameObject prefab) {
		GameObject fx = Instantiate(prefab, transform.position, prefab.transform.rotation);
		Destroy(fx, particleFxDuration);
	}

	void DestroyPickup() {
		meshRandomiser.DestroyMesh();
		pool.Release(this);
	}

	void SpawnNewPickup() {
		pickupSpawner.SpawnPickup();
	}
}
