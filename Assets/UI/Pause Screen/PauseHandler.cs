using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PauseHandler : MonoBehaviour {

	[SerializeField] float canvasFadeSpeed = 4f;
	[SerializeField] CanvasFadeInOut audioCanvasFade;

	CanvasFadeInOut pauseCanvasFade;

	bool canPause = false;
	bool isPaused = false;

	void Awake() {
		pauseCanvasFade = GetComponent<CanvasFadeInOut>();
	}

	void Start() {
		pauseCanvasFade.StartFadeOut(Mathf.Infinity);
		LaunchSession session = FindObjectOfType<LaunchSession>();
		if (session == null) return;
		canPause = session.HasCompletedMainMenu;
	}

	void Update() {
		if (!canPause) return;
		HandlePauseButton();
	}

	public void SetCanPause(bool canPause) {
		this.canPause = canPause;
	}

	void HandlePauseButton() {
		if (Input.GetButtonDown(Constants.Input.pause)) {
			if (isPaused) {
				UnpauseGame();
			}
			else {
				PauseGame();
			}
		}
	}

	void PauseGame() {
		isPaused = true;
		Time.timeScale = 0f;
		pauseCanvasFade.StartFadeIn(canvasFadeSpeed);
		audioCanvasFade.StartFadeIn(canvasFadeSpeed);
	}

	void UnpauseGame() {
		audioCanvasFade.StartFadeOut(canvasFadeSpeed);
		pauseCanvasFade.StartFadeOut(canvasFadeSpeed, FinishUnpausingGame);
	}

	void FinishUnpausingGame() {
		Time.timeScale = 1f;
		isPaused = false;
	}
}
