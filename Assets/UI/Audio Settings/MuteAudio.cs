using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuteAudio : MonoBehaviour {

	[SerializeField] Sprite soundOnSprite;
	[SerializeField] Sprite soundOffSprite;
	[SerializeField] Image image;

	void Start() {
		UpdateSprite();
	}

	void Update() {
		if (Input.GetButtonDown(Constants.Input.mute)) {
			ToggleMute();
		}
	}

	public void ToggleMute() {
		AudioListener.pause = !AudioListener.pause;
		UpdateSprite();
	}

	void UpdateSprite() {
		image.sprite = AudioListener.pause ? soundOffSprite : soundOnSprite;
	}
}
