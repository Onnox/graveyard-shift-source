using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions {
    
    public static Element RandomElement<Element>(this List<Element> list) {
        return list[Random.Range(0, list.Count - 1)];
	}

    public static Element RandomElement<Element>(this Element[] array) {
        return array[Random.Range(0, array.Length - 1)];
    }

    public static void PlayOneShotIfNotMuted(this AudioSource audioSource, AudioClip clip) {
        if (AudioListener.pause) return;
        audioSource.PlayOneShot(clip);
	}

    public static void PlayIfNotMuted(this AudioSource audioSource) {
        if (AudioListener.pause) return;
        audioSource.Play();
    }
}
