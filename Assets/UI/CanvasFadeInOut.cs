using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class CanvasFadeInOut : MonoBehaviour {

	[SerializeField] bool shouldShowOnFirstRun;
	[SerializeField] bool shouldShowOnSubsequentRuns;

	CanvasGroup canvasGroup;

	void Awake() {
		canvasGroup = GetComponent<CanvasGroup>();
	}

	void Start() {
		ShowOrHideIfNeeded();
	}

	void ShowOrHideIfNeeded() {
		LaunchSession session = FindObjectOfType<LaunchSession>();
		if (session == null) return;
		if (session.HasCompletedMainMenu) {
			if (shouldShowOnSubsequentRuns) {
				StartFadeIn(Mathf.Infinity);
			}
			else {
				FinishFadeOut();
			}
		}
		else {
			if (shouldShowOnFirstRun) {
				StartFadeIn(Mathf.Infinity);
			}
			else {
				FinishFadeOut();
			}
		}
	}

	public void StartFadeIn(float fadeSpeed) {
		StartFadeIn(fadeSpeed, null);
	}

	public void StartFadeIn(float fadeSpeed, Action onCompleted) {
		
		foreach (Transform child in transform) {
			child.gameObject.SetActive(true);
		}
		
		StopAllCoroutines();
		StartCoroutine(FadeInCanvas(fadeSpeed, onCompleted));
	}

	IEnumerator FadeInCanvas(float fadeSpeed, Action onCompleted) {
		
		while (canvasGroup.alpha < 1) {
			canvasGroup.alpha += fadeSpeed * Time.unscaledDeltaTime;
			yield return new WaitForEndOfFrame();
		}

		onCompleted?.Invoke();
	}

	public void StartFadeOut(float fadeSpeed) {
		StartFadeOut(fadeSpeed, null);
	}

	public void StartFadeOut(float fadeSpeed, Action onCompleted) {
		StopAllCoroutines();
		StartCoroutine(FadeOutCanvas(fadeSpeed, onCompleted));
	}

	IEnumerator FadeOutCanvas(float fadeSpeed, Action onCompleted) {

		while (canvasGroup.alpha > 0) {
			canvasGroup.alpha -= fadeSpeed * Time.unscaledDeltaTime;
			yield return new WaitForEndOfFrame();
		}

		FinishFadeOut(onCompleted: onCompleted);
	}

	protected virtual void FinishFadeOut(Action onCompleted = null) {
		
		canvasGroup.alpha = 0;
		
		foreach (Transform child in transform) {
			child.gameObject.SetActive(false);
		}

		onCompleted?.Invoke();
	}
}
