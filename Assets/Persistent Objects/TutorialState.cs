using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialState : MonoBehaviour {

	public bool HasCollectedLoot { get; set; } = false;
	public bool HasKilledEnemies { get; set; } = false;

	public bool ShouldStartEnemyTutorialSequence {
		get { return HasCollectedLoot && !HasKilledEnemies; }
	}
}
