using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SummonFX : MonoBehaviour {

	[SerializeField] GameObject fxPrefab;
	[SerializeField] float destroyDelay;

	void OnEnable() {
		GameObject summonFX = Instantiate(fxPrefab, transform);
		Destroy(summonFX, destroyDelay);
	}
}
