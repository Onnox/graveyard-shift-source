using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ScoreBag : MonoBehaviour {

	[SerializeField] Image star;
	[SerializeField] TextMeshProUGUI scoreText;

	public void SetScore(int score) {
		scoreText.text = score.ToString();
	}

	public void SetStarActive(bool active) {
		star.gameObject.SetActive(active);
	}
}
