using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	readonly string isRunningParam = Constants.AnimatorParams.Player.isRunning;

	[SerializeField] float movementSpeed = 5f;
	[SerializeField] Animator animator;

	bool canMove = true;

	Rigidbody rb;

	private void Start() {
		rb = GetComponent<Rigidbody>();
		DecideIfCanMove();
	}

	void DecideIfCanMove() {
		LaunchSession session = FindObjectOfType<LaunchSession>();
		if (session == null) return;
		canMove = session.HasCompletedMainMenu;
	}

	void FixedUpdate() {
		if (!canMove) return;
		HandleInput();
	}

	void HandleInput() {
		float horizontalInput = Input.GetAxis(Constants.Input.horizontal);
		float verticalInput = Input.GetAxis(Constants.Input.vertical);

		if (Mathf.Abs(horizontalInput) < Mathf.Epsilon && Mathf.Abs(verticalInput) < Mathf.Epsilon) {
			animator.SetBool(isRunningParam, false);
			return;
		}

		animator.SetBool(isRunningParam, true);
		HandleMovement(horizontalInput, verticalInput);
		HandleRotation(horizontalInput, verticalInput);
	}

	void HandleRotation(float horizontalInput, float verticalInput) {
		Vector3 lookVector = new Vector3(horizontalInput, 0, verticalInput).normalized;
		transform.rotation = Quaternion.LookRotation(lookVector);
	}

	void HandleMovement(float horizontalInput, float verticalInput) {

		Vector3 moveVector = Vector3.ClampMagnitude(new(horizontalInput, 0, verticalInput), 1f);
		animator.SetFloat(Constants.AnimatorParams.Player.runAmount, moveVector.magnitude);

		Vector3 movementThisFrame = movementSpeed * Time.fixedDeltaTime * moveVector;
		rb.MovePosition(transform.position + movementThisFrame);
	}

	public void EnableMovement() {
		canMove = true;
	}
}
