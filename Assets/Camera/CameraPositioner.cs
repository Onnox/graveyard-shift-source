using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public enum CameraMoveType {
	FromMainMenu,
	Retry
}

public class CameraPositioner : MonoBehaviour {

	[SerializeField] CinemachineVirtualCamera mainMenuVirtualCamera;
	[SerializeField] CinemachineVirtualCamera gameVirtualCamera;

	[Header("Audio")]
	[SerializeField] AudioClip mainMenuTransitionAudioClip;
	[SerializeField] float mainMenuTransitionAudioClipVolume;

	[Space]
	[SerializeField] AudioClip retryAudioClip;
	[SerializeField] float retryAudioClipVolume;

	AudioSource audioSource;

	void Awake() {
		audioSource = GetComponent<AudioSource>();
	}

	void Start() {
		DecideStartPosition();
	}

	void DecideStartPosition() {
		LaunchSession session = FindObjectOfType<LaunchSession>();
		if (session == null) return;
		if (!session.HasCompletedMainMenu) {
			MoveToMainMenuPosition();
		}
		else {
			MoveToGamePosition(CameraMoveType.Retry);
		}
	}

	void MoveToMainMenuPosition() {
		gameVirtualCamera.enabled = false;
		mainMenuVirtualCamera.enabled = true;
	}

	public void MoveToGamePosition(CameraMoveType cameraMoveType) {

		mainMenuVirtualCamera.enabled = false;
		gameVirtualCamera.enabled = true;

		switch (cameraMoveType) {
		case CameraMoveType.FromMainMenu:
			PlayAudioClip(mainMenuTransitionAudioClip, mainMenuTransitionAudioClipVolume);
			break;
		case CameraMoveType.Retry:
			PlayAudioClip(retryAudioClip, retryAudioClipVolume);
			break;
		default:
			break;
		}
	}

	void PlayAudioClip(AudioClip clip, float volume) {
		audioSource.volume = volume;
		audioSource.PlayOneShotIfNotMuted(clip);
	}
}
