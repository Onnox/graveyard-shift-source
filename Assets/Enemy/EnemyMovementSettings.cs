using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy Movement Settings", menuName = "ScriptableObjects/Enemy Movement Settings", order = 1)]
public class EnemyMovementSettings : ScriptableObject {
	public float summonDuration = 1.75f;
	public float baseSpeed = 20f;
	public float speedRamp = 1.5f;
	public float animatorSpeedDivision = 4f;
}
