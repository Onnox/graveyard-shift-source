using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherEnemyDetection : MonoBehaviour {

	private void OnTriggerEnter(Collider other) {
		OtherEnemyDetection otherEnemyDetection = other.GetComponent<OtherEnemyDetection>();
		if (otherEnemyDetection == null) return;
		GetComponentInParent<Enemy>().Kill();
	}
}
