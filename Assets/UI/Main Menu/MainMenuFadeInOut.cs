using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuFadeInOut : CanvasFadeInOut {

	override protected void FinishFadeOut(Action onCompleted = null) {
		
		base.FinishFadeOut(onCompleted);

		LaunchSession session = FindObjectOfType<LaunchSession>();
		if (session != null) {
			session.HasCompletedMainMenu = true;
		}

		Destroy(gameObject);
	}
}
