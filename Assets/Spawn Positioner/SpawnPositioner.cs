using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnPositioner : MonoBehaviour {

	[SerializeField] Transform[] spawnPoints;

	[Tooltip("Don't reuse a spawn position if it has already recently been used within this many spawns.")]
	[SerializeField] int dontRepeatTheLastXSpawns = 8;

	[Space]

	[SerializeField] float mininumDistanceFromPlayer = 5f;

	[Tooltip("For checking distance to player")]
	[SerializeField] Transform player;

	Queue<Transform> recentSpawnPoints;

	void Awake() {
		recentSpawnPoints = new(dontRepeatTheLastXSpawns);
	}

#if DEBUG
	void OnDrawGizmos() {
		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere(player.position, mininumDistanceFromPlayer);
	}
#endif

	public Vector3 GetSpawnPosition(bool shouldAvoidPlayer) {

		List<Transform> availableSpawnPoints = spawnPoints.ToList();

		foreach (Transform recent in recentSpawnPoints) {
			availableSpawnPoints.Remove(recent);
		}

		if (shouldAvoidPlayer) {
			availableSpawnPoints = SafelyRemovePointsTooCloseToPlayer(availableSpawnPoints);
		}

		Transform randomSpawnPoint = availableSpawnPoints.RandomElement();

		if (recentSpawnPoints.Count >= dontRepeatTheLastXSpawns) {
			recentSpawnPoints.Dequeue();
		}

		recentSpawnPoints.Enqueue(randomSpawnPoint);
		return randomSpawnPoint.position;
	}

	List<Transform> SafelyRemovePointsTooCloseToPlayer(List<Transform> points) {

		List<Transform> pointsNotTooClose = new(points);

		pointsNotTooClose.RemoveAll(point => Vector3.Distance(point.position, player.position) < mininumDistanceFromPlayer);

		if (pointsNotTooClose.Count > 0) {
			return pointsNotTooClose;
		}
		else {
			return points;
		}
	}
}
