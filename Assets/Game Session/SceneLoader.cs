using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class SceneLoader {

    public static void RestartGame() {
        SceneManager.LoadScene(0);
        Time.timeScale = 1f;
    }
}
