using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StartButtonHandler : MonoBehaviour {

	[Header("On Start button pressed")]
	[SerializeField] float canvasFadeSpeed = 1f;
	[SerializeField] CameraPositioner cameraPositioner;
	[SerializeField] CanvasFadeInOut audioCanvasFade;

	[Header("Audio")]
	[SerializeField] AudioSource audioSource;
	[SerializeField] AudioClip startAudioClip;
	[SerializeField] float volume = 0.25f;

	[Header("On Finish Fade Out")]
	[SerializeField] PauseHandler pauseHandler;
	[SerializeField] TutorialSequence tutorialSequence;
	[SerializeField] PickupSpawner pickupSpawner;
	[SerializeField] CanvasFadeInOut scoreCanvasFade;
	[SerializeField] PlayerMovement playerMovement;

	MainMenuFadeInOut mainMenuCanvasFade;

	bool canStart = true;

	void Awake() {
		mainMenuCanvasFade = GetComponent<MainMenuFadeInOut>();
	}

	void Update() {
		if (!canStart) return;
		HandlePressStart();
	}

	public void SetCanStart(bool canStart) {
		this.canStart = canStart;
	}

	void HandlePressStart() {
		if (Input.GetButtonDown(Constants.Input.submit)) {
			mainMenuCanvasFade.StartFadeOut(canvasFadeSpeed, OnFinishFadeOut);
			audioSource.volume = volume;
			audioSource.PlayOneShotIfNotMuted(startAudioClip);
			cameraPositioner.MoveToGamePosition(CameraMoveType.FromMainMenu);
			audioCanvasFade.StartFadeOut(canvasFadeSpeed);
			enabled = false;
		}
	}

	void OnFinishFadeOut() {
		pauseHandler.SetCanPause(true);
		tutorialSequence.StartLootTutorialSequence();
		pickupSpawner.SpawnFirstPickup();
		scoreCanvasFade.StartFadeIn(canvasFadeSpeed);
		playerMovement.EnableMovement();
	}
}
