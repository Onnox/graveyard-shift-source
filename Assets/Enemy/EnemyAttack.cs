using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour {

	[SerializeField] Animator animator;

	void OnTriggerEnter(Collider other) {
		DeathHandler deathHandler = other.GetComponent<DeathHandler>();
		if (deathHandler == null) return;
		animator.SetTrigger(Constants.AnimatorParams.Enemy.attack);
		deathHandler.HandleDeath();
	}
}
