# Graveyard Shift Source

Submission for The GameDev.tv Game Jam 2022 - https://itch.io/jam/gamedevtv-jam-2022

A simple game in a 2D environment about a pumpkin collecting sweets in a graveyard while dodging skeletons.

## Usage
Created in Unity version 2021.3.3f1

## Authors and acknowledgment

### UI
Speaker icon & Speaker off icon by Delapouite https://delapouite.com/ 
CC BY 3.0 https://creativecommons.org/licenses/by/3.0/

Shiny iris icon by Lorc https://lorcblog.blogspot.com/
CC BY 3.0 https://creativecommons.org/licenses/by/3.0/ - modified to fill space from original

UI pack: RPG extension by Kenney Vleugels www.kenney.nl
CC0 1.0 http://creativecommons.org/publicdomain/zero/1.0/

Bones font by Carolina Plaza
https://www.dafont.com/bonesfont.font

### 3D Assets
Ultimate Nature Pack by Quaternius
https://quaternius.com/packs/ultimatenature.html
CC0 1.0 http://creativecommons.org/publicdomain/zero/1.0/

KayKit Character Animations, KayKit Character Pack - Skeletons, KayKit Spooktober Seasonal Pack by Kay Lousberg
CC0 1.0 http://creativecommons.org/publicdomain/zero/1.0/

Stylized Ground Texture by Jairo Alzate
https://www.behance.net/gallery/97112307/Stylized-Ground-Free-Download

### Audio
Ghost Town by Rafael Krux 
https://freepd.com/comedy.php
CC0 1.0 http://creativecommons.org/publicdomain/zero/1.0/

Night village edge with away storm by Guz99
https://freesound.org/people/Guz99/sounds/583740/
CC0 1.0 http://creativecommons.org/publicdomain/zero/1.0/

Earth rumble by Reitanna
https://freesound.org/people/Reitanna/sounds/217657/
CC0 1.0 http://creativecommons.org/publicdomain/zero/1.0/

Body fall v hvy dirt by leonelmail
https://freesound.org/people/leonelmail/sounds/504626/
CC0 1.0 http://creativecommons.org/publicdomain/zero/1.0/

Avl02 by blaukreuz
https://freesound.org/people/blaukreuz/sounds/162670/
CC0 1.0 http://creativecommons.org/publicdomain/zero/1.0/

Blip-plock-pop by onikage22
https://freesound.org/people/onikage22/sounds/240566/
CC0 1.0 http://creativecommons.org/publicdomain/zero/1.0/

Falling bones by spookymodem
https://freesound.org/people/spookymodem/sounds/202091/
CC0 1.0 http://creativecommons.org/publicdomain/zero/1.0/

d0_step_on_egg01, d0_step_on_egg03, d0_step_on_egg04 by dav0r CC0 1.0 http://creativecommons.org/publicdomain/zero/1.0/
- https://freesound.org/people/dav0r/sounds/176759/
- https://freesound.org/people/dav0r/sounds/176761/
- https://freesound.org/people/dav0r/sounds/176760/

bec low bell solo by HMTSCCSound
https://freesound.org/people/HMTSCCSound/sounds/554655/
CC0 1.0 http://creativecommons.org/publicdomain/zero/1.0/

Witch (laughing) by ZyryTSounts
https://freesound.org/people/ZyryTSounds/sounds/237984/
CC0 1.0 http://creativecommons.org/publicdomain/zero/1.0/

Cartoon laugh by JohnsonBrandEditing
https://freesound.org/people/JohnsonBrandEditing/sounds/173933/
CC0 1.0 http://creativecommons.org/publicdomain/zero/1.0/

Whoosh_long_low by DJT4NN3R
https://freesound.org/people/DJT4NN3R/sounds/449990/
CC0 1.0 http://creativecommons.org/publicdomain/zero/1.0/

### Scripts
LightFlickerEffect.cs by Steve Streeting
CC0 1.0 http://creativecommons.org/publicdomain/zero/1.0/

OpenHyperlinks.cs by Jonathan Hopkins
https://gitlab.com/jonnohopkins/tmp-hyperlinks
