using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class EnemySpawner : MonoBehaviour {

    [SerializeField] Enemy enemyPrefab;
    [SerializeField] SpawnPositioner spawnPositioner;
    [SerializeField] TutorialSequence tutorialSequence;

    IObjectPool<Enemy> enemyPool;

    void Awake() {
        enemyPool = new ObjectPool<Enemy>(
            OnCreateEnemy, 
            OnGetEnemy,
            OnReleaseEnemy, 
            OnDestroyEnemy, 
            maxSize: 50
        );
    }

    public void SpawnEnemy() {
        // Get position first because doing Get() first will constrain the spawn position more than necessary.
        Vector3 randomSpawnPosition = spawnPositioner.GetSpawnPosition(shouldAvoidPlayer: true);
        Enemy enemy = enemyPool.Get();
        enemy.transform.position = randomSpawnPosition;

        StartEnemyTutorialIfNeeded();
	}

    void StartEnemyTutorialIfNeeded() {
        if (transform.childCount > 1 && tutorialSequence.ShouldStartEnemyTutorialSequence) {
            tutorialSequence.StartEnemyTutorialSequence();
        }
    }

	#region ObjectPool methods

	Enemy OnCreateEnemy() {
        Enemy enemy = Instantiate(enemyPrefab, transform);
        enemy.Pool = enemyPool;
        return enemy;
	}

    void OnGetEnemy(Enemy enemy) {
        enemy.gameObject.SetActive(true);
	}

    void OnReleaseEnemy(Enemy enemy) {
        enemy.gameObject.SetActive(false);
	}

    void OnDestroyEnemy(Enemy enemy) {
        Destroy(enemy.gameObject);
	}

	#endregion
}
