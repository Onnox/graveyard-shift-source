using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class PickupSpawner : MonoBehaviour {

    [SerializeField] Pickup pickupPrefab;
    [SerializeField] SpawnPositioner spawnPositioner;
    [SerializeField] float spawnDistanceOffGround = 0.75f;
    [SerializeField] float firstPickupDelay = 1.5f;

    IObjectPool<Pickup> pickupPool;

    void Awake() {
        pickupPool = new ObjectPool<Pickup>(
            OnCreatePickup,
            OnGetPickup,
            OnReleasePickup,
            OnDestroyPickup,
            maxSize: 3
        );
    }

	void Start() {
        LaunchSession session = FindObjectOfType<LaunchSession>();
        if (session == null) return;
        if (session.HasCompletedMainMenu) {
            SpawnFirstPickup();
        }
    }

    public void SpawnFirstPickup() {
        SpawnPickup(firstPickupDelay);
	}

    public void SpawnPickup() {
        SpawnPickup(0f);
	}

	void SpawnPickup(float delay) {
        StartCoroutine(SpawnPickupAfterDelay(delay));
    }

    IEnumerator SpawnPickupAfterDelay(float delay) {
        yield return new WaitForSeconds(delay);
        Vector3 randomPosition = spawnPositioner.GetSpawnPosition(shouldAvoidPlayer: false);
        randomPosition += new Vector3(0f, spawnDistanceOffGround, 0f);
        Pickup pickup = pickupPool.Get();
        pickup.transform.position = randomPosition;
        pickup.CreateSpawnParticleFx();
    }

    #region ObjectPool methods

    Pickup OnCreatePickup() {
        Pickup pickup = Instantiate(pickupPrefab, transform);
        pickup.Pool = pickupPool;
        return pickup;
    }

    void OnGetPickup(Pickup pickup) {
        pickup.gameObject.SetActive(true);
    }

    void OnReleasePickup(Pickup pickup) {
        pickup.gameObject.SetActive(false);
    }

    void OnDestroyPickup(Pickup pickup) {
        Destroy(pickup.gameObject);
    }

    #endregion
}
