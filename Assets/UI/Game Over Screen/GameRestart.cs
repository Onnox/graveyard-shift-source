using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRestart : MonoBehaviour {

	[SerializeField] SceneFade sceneFade;

	bool canRestart = false;

	void Update() {
		if (!canRestart) return;
		if (Input.GetButtonDown(Constants.Input.submit)) {
			sceneFade.FadeIn(SceneLoader.RestartGame);
		}
	}

	public void SetCanRestart(bool canRestart) {
		this.canRestart = canRestart;
	}
}
