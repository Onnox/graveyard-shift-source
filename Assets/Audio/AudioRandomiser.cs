using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioRandomiser : MonoBehaviour {

    [SerializeField] AudioClip[] audioClips;

    AudioSource audioSource;

	void Awake() {
		audioSource = GetComponent<AudioSource>();
	}

	public void PlayRandomAudioClip() {
		audioSource.PlayOneShotIfNotMuted(audioClips.RandomElement());
	}
}
