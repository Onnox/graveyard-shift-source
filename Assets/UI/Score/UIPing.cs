using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class UIPing : MonoBehaviour {

	Animator animator;

	private void Awake() {
		animator = GetComponent<Animator>();
	}

	public void Ping() {
		animator.SetTrigger(Constants.AnimatorParams.UI.ping);
	}
}
