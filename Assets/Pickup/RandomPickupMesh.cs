using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPickupMesh : MonoBehaviour {

	[SerializeField] GameObject[] meshPrefabs;

	GameObject instantiatedMesh;

	void OnEnable() {
		int randomIndex = Random.Range(0, meshPrefabs.Length - 1);
		instantiatedMesh = Instantiate(meshPrefabs[randomIndex], transform);
		instantiatedMesh.transform.localScale = new Vector3(2f, 2f, 2f);
	}

	public void DestroyMesh() {
		Destroy(instantiatedMesh);
	}
}
