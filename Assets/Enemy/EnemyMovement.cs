using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyMovement : MonoBehaviour {

	[SerializeField] EnemyMovementSettings settings;
	[SerializeField] Animator animator;

	Enemy enemy;
	Transform target;
	NavMeshAgent navMeshAgent;
	bool canMove = false;

	void Start() {
		enemy = GetComponent<Enemy>();
		target = GameObject.FindWithTag(Constants.Tags.player).transform;
		navMeshAgent = GetComponent<NavMeshAgent>();
	}

	void OnEnable() {
		StartCoroutine(DoSummon());
	}

	IEnumerator DoSummon() {
		yield return new WaitForSeconds(settings.summonDuration);
		canMove = true;
		navMeshAgent.enabled = true;
		enemy.SetCollidersEnabled(true);
	}

	void Update() {
		if (!canMove) return;
		navMeshAgent.SetDestination(target.position);
		UpdateSpeed();
	}

	void UpdateSpeed() {
		float distanceToTarget = Vector3.Distance(transform.position, target.position);
		float newSpeed = settings.baseSpeed / Mathf.Pow(distanceToTarget, settings.speedRamp);
		navMeshAgent.speed = newSpeed;
		animator.SetFloat(Constants.AnimatorParams.Enemy.runAmount, newSpeed / settings.animatorSpeedDivision);
	}

	public void DisableMovement() {
		StopAllCoroutines();
		canMove = false;
		navMeshAgent.enabled = false;
	}

	public static void DisableAllEnemyMovementAndStartTaunt() {

		EnemyMovement[] enemyMovements = FindObjectsOfType<EnemyMovement>();

		foreach (EnemyMovement enemyMovement in enemyMovements) {
			enemyMovement.DisableMovement();
			enemyMovement.animator.SetTrigger(Constants.AnimatorParams.Enemy.taunt);
		}
	}
}
